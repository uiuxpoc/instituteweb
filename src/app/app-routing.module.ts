import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { LandingPageComponent } from './AccountApp/LandingPage/landingPage.component';
import { SignupComponent } from './AccountApp/SignupPage/signup.component';
import { LoginComponent } from './AccountApp/LoginPage/login.component';
import { UserMasterComponent } from './AdminApp/UserMaster/usermaster.component';
import { ConfirmMobileComponent } from './AccountApp/ConfirmMobile/confirm-mobile.component';
import { NavigationComponent } from './UtilityApp/Navigation/navigation.component';
import { TopnavbarComponent } from './UtilityApp/Topnavbar/topnavbar.component';


//Dashboard & internal pages
import { DashboardComponent } from './DashboardApp/Dashboard/dashboard.component'
import { AttendanceComponent } from './DashboardApp/Attendance/attendance.component';
import { AddEmployeeComponent } from './DashboardApp/Employees/Add/addemployee.component';
import { ManageEmployeeComponent } from './DashboardApp/Employees/manage/manageemployee.component';
import { FinanceAssetManagementComponent } from './DashboardApp/Finance/AssetManagement/financeassetmanagement.component';
import { FinanceDonationComponent } from './DashboardApp/Finance/Donations/financedonation.component';
import { FinanceFeeComponent } from './DashboardApp/Finance/Fees/financefee.component';
import { FinancePayslipComponent } from './DashboardApp/Finance/Payslip/financepayslip.component';
import { FinanceReportComponent } from './DashboardApp/Finance/Report/financereport.component';
import { FinanceCategoryComponent } from './DashboardApp/Finance/Category/financecategory.component';
import { FinanceTransactionComponent } from './DashboardApp/Finance/Transaction/financetransaction.component';
import { HostelFeeCollectionComponent } from './DashboardApp/Hostel/FeeCollection/hostelfeecollection.component';
import { HostelAdditionDetailComponent } from './DashboardApp/Hostel/AdditionalDetail/hosteladditionaldetail.component';
import { HostelFeeDefaulterComponent } from './DashboardApp/Hostel/FeeDefaulter/hostelfeedefaulter.component';
import { HostelFeePayComponent } from './DashboardApp/Hostel/FeePay/hostelfeepay.component';
import { HostelReportComponent } from './DashboardApp/Hostel/Report/hostelreport.component';
import { HostelRoomAdditionalDetailComponent } from './DashboardApp/Hostel/RoomAdditionalDetail/hostelroomadditionaldetail.component';
import { HostelRoomAllocationComponent } from './DashboardApp/Hostel/RoomAllocation/hostelroomallocation.component';
import { AddSchoolComponent}  from './DashboardApp/School/Add/addschool.component';
import { SchoolListComponent } from './DashboardApp/School/List/schoollist.component';
import { SettingsComponent } from './DashboardApp/Settings/settings.component';
import { AcadamicSettingsComponent } from './DashboardApp/Settings/Acadamic/acadamicsettings.component';
import { EmployeeSettingsComponent } from './DashboardApp/Settings/Employee/employeesettings.component';
import { GeneralSettingsComponent } from './DashboardApp/Settings/General/generalsettings.component';
import { PaymentSettingsComponent } from './DashboardApp/Settings/Payment/paymentsettings.component';
import { StudentSettingsComponent } from './DashboardApp/Settings/Student/studentsettings.component';
import { AddStudentComponent } from './DashboardApp/Students/Add/addstudent.component';
import { StudentListComponent } from './DashboardApp/Students/List/studentlist.component';
import { TimetableComponent } from './DashboardApp/TimeTable/timetable.component';
import { TransportFeeComponent } from './DashboardApp/Transport/Fee/transportfee.component';
import { TransportManageRouteComponent } from './DashboardApp/Transport/ManageRoute/transportmanageroute.component';
import { TransportManageVehicleComponent } from './DashboardApp/Transport/ManageVehicle/transportmanagevehicle.component';
import { TransportReportComponent } from './DashboardApp/Transport/Report/transportreport.component';
import { TransportSetRouteComponent } from './DashboardApp/Transport/SetRoutes/transportsetroute.component';





const routes: Routes = [
    { path: '', redirectTo: '/index', pathMatch: 'full' },
    { path: 'index', component: LandingPageComponent },
    { path: 'login', component: LoginComponent },
    { path: 'signup', component: SignupComponent },
    { path: 'confirmMobile', component: ConfirmMobileComponent },
    { path: 'userMaster', component: UserMasterComponent },
    { path: 'navigation', component: NavigationComponent },
    { path: 'topnavbar', component: TopnavbarComponent },
    { path: 'attendance', component: AttendanceComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'addemployee', component: AddEmployeeComponent },
    { path: 'manageemployee', component: ManageEmployeeComponent },
    { path: 'assetmanagement', component: FinanceAssetManagementComponent },
    { path: 'financategory', component: FinanceCategoryComponent },
    { path: 'finandonation', component: FinanceDonationComponent },
    { path: 'financefee', component: FinanceFeeComponent },
    { path: 'financepayslip', component: FinancePayslipComponent },
    { path: 'financereport', component: FinanceReportComponent },
    { path: 'financetransaction', component: FinanceTransactionComponent },
    { path: 'hosteadditiondetail', component: HostelAdditionDetailComponent },
    { path: 'hostelfeecollection', component: HostelFeeCollectionComponent },
    { path: 'hostelfeedefauleter', component: HostelFeeDefaulterComponent },
    { path: 'hostelfeepay', component: HostelFeePayComponent },
    { path: 'hostelreport', component: HostelReportComponent },
    { path: 'hostelroomadditionaldetail', component: HostelRoomAdditionalDetailComponent },
    { path: 'hostelroomallocation', component: HostelRoomAllocationComponent },
    { path: 'addschool', component: AddSchoolComponent },
    { path: 'schoollist', component: SchoolListComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'acadamicsettings', component: AcadamicSettingsComponent },
    { path: 'employeesettings', component: EmployeeSettingsComponent },
    { path: 'generalsettings', component: GeneralSettingsComponent },
    { path: 'paymentsettings', component: PaymentSettingsComponent },
    { path: 'studentsettings', component: StudentSettingsComponent },
    { path: 'addstudent', component: AddStudentComponent },
    { path: 'studentlist', component: StudentListComponent },
    { path: 'timetable', component: TimetableComponent },
    { path: 'transportfee', component: TransportFeeComponent },
    { path: 'transportmanageroute', component: TransportManageRouteComponent },
    { path: 'transportmanagevehicle', component: TransportManageVehicleComponent },
    { path: 'transportreport', component: TransportReportComponent },
    { path: 'transportsetroute', component: TransportSetRouteComponent },


];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
