import { Component, OnInit } from '@angular/core';
import { Login, Registration, CheckMobileNumber, VerifyUserPhone } from '../Models/account';
import { ConfirmMobleService } from '../Services/confirm-mobile.service';
import { Router } from "@angular/router";
import { LocalstorageService } from '../../UtilityApp/Services/localstorage.service';
import { SharedService } from '../../UtilityApp/Services/shared.service';
import { NotificationService } from '../../UtilityApp/Services/notification.service';

@Component({
  selector: 'confirm-mobile',
  templateUrl: './confirm-mobile.component.html'
})

export class ConfirmMobileComponent {
  otpCode: number;
  verifyUser: VerifyUserPhone = new VerifyUserPhone();

  constructor(private confirmMobleService: ConfirmMobleService, private router: Router,
    private sharedService: SharedService, private localstorageService: LocalstorageService,
    private notificationService: NotificationService) {
    localstorageService.removeIteamData('userData');

    if (this.sharedService.getDataByKey("mobileNumber") != undefined && this.sharedService.getDataByKey("mobileNumber") != null) {
      this.verifyUser.userid = JSON.parse(this.sharedService.getDataByKey("mobileNumber"));
    }

    this.otpCode = Math.floor(100000 + Math.random() * 900000);

    // this.otpCode = localstorageService.getData("otp");
    // // this.notificationService.success("Your OTP code is", this.otpCode);
    // console.log(localstorageService.getData("otp"));

  }

  VerifyUserPhone(verifyUserPhoneModel: VerifyUserPhone) {
    debugger
    

   if(this.otpCode.toString() == verifyUserPhoneModel.code){
    this.sharedService.setData("mobileNumber", this.verifyUser.userid);
    this.sharedService.setData("password", JSON.parse(this.sharedService.getDataByKey("password")));
    this.sharedService.setData("domainName", JSON.parse(this.sharedService.getDataByKey("domainName")));
    this.notificationService.success("Welcome", "Your Registeration is completed"); 

    this.router.navigate(['login']);
   }
    else{
     this.notificationService.success("Incorrect OTP", "Please enter valid OTP code."); 
    }



    // this.confirmMobleService.verifyUserPhone(verifyUserPhoneModel)
    //   .then(result => {
    //     debugger
    //     if (result.status == 200) {
    //       console.log(this.confirmMobleService.checkStatusCode(result.status));
    //       this.localstorageService.setData('userData', result.data);
    //       this.router.navigate(['login']);
    //     }
    //     else {
    //       console.log(this.confirmMobleService.checkStatusCode(result.status));
    //     }

    //   });
  }

  resendCode(verifyUserPhoneModel: VerifyUserPhone) {
    this.notificationService.success("Phone number confirmation", "Code is send to your phone number");
    // this.confirmMobleService.verifyUserPhone(verifyUserPhoneModel)
    //   .then(result => {
    //     debugger
    //     if (result.status == 200) {
    //       console.log(this.confirmMobleService.checkStatusCode(result.status));
    //       this.localstorageService.setData('userData', result.data);
    //       this.router.navigate(['login']);
    //     }
    //     else {
    //       console.log(this.confirmMobleService.checkStatusCode(result.status));
    //     }

    //   });
  }

}