import { Component } from '@angular/core';
import { SharedService } from '../../../UtilityApp/Services/shared.service';

@Component({
  selector: 'studentsettings',
  templateUrl: './studentsettings.component.html'
})
export class StudentSettingsComponent {
  //title = 'app works!';
  constructor(private shared:SharedService){
          this.shared.emitTypeBroadcast();
        }
}
