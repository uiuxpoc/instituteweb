import { Component } from '@angular/core';
import { SharedService } from '../../../UtilityApp/Services/shared.service';

@Component({
  selector: 'employeesettings',
  templateUrl: './employeesettings.component.html'
})
export class EmployeeSettingsComponent {
  //title = 'app works!';
  constructor(private shared:SharedService){
          this.shared.emitTypeBroadcast();
        }
}
