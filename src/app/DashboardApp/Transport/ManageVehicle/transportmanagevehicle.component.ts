import { Component } from '@angular/core';
import { SharedService } from '../../../UtilityApp/Services/shared.service';

@Component({
  selector: 'transportmanagevehicle',
  templateUrl: './transportmanagevehicle.component.html'
})
export class TransportManageVehicleComponent {
  //title = 'app works!';
  constructor(private shared:SharedService){
          this.shared.emitTypeBroadcast();
        }
}
