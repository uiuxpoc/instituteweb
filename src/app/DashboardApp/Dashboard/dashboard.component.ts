import { Component } from '@angular/core';
import { AddEmployeeComponent} from '../../UtilityApp/AddEmployee/addemployee.component';
import { SharedService } from '../../UtilityApp/Services/shared.service';

@Component({
  selector:'dashboard',
  templateUrl : './dashboard.component.html'
})

export class DashboardComponent {
  
constructor(private shared:SharedService                  
        ){
          this.shared.emitTypeBroadcast();                    
        }        

}

